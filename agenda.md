# Mosaic Team Retreat - Agenda

## Towards High(er) Performance Computing

### Wednesday 18/10
* <span style="color:cornflowerblue">09:00-09:30 - *Welcome*</span>
* 09:30-10:00 - **Introduction** - When to optimize a program and how? [Ali Farnudi]
* 10:00-10:30 - **Optimize First** - Choosing appropriate data structures: list vs array [Ali Farnudi]
* <span style="color:cornflowerblue">10:30-11:00 - *Coffee Break*</span>
* 11:00-12:00 - **Optimize First** - Complexity and optimization: what makes a program slow? [Arthur Luciani]
* 12:00-12:30 - **Optimize First** - Profiling: looking for computational bottlenecks [Guillaume Cerutti]
* <span style="color:cornflowerblue">12:30-14:00 - *Lunch Break*</span>
* <span style="color:palegreen"><!-- .element: class="fragment" data-fragment-index="1" -->14:00-15:30 - *__Hands-on__* - Testing profiling tools on my own programs</span>
* <span style="color:cornflowerblue"><!-- .element: class="fragment" data-fragment-index="1" -->15:30-16:00 - *Coffee Break*</span>
* <span style="color:gold"><!-- .element: class="fragment" data-fragment-index="2" -->16:00-16:30 - **_Meta-scientific activity_** - TBA [Christophe Godin]</span>
* <span style="color:gold"><!-- .element: class="fragment" data-fragment-index="2" -->16:30-17:30 - **_Meta-scientific activity_** - Note-taking / journaling: practices and tools [Guillaume Cerutti]</span>
* <span style="color:orangered"><!-- .element: class="fragment" data-fragment-index="3" -->18:00-21:30 - **_Social activity_** - Dinner & Games @[Tire-toi une bûche](https://maps.app.goo.gl/uHNGH8cWT6catk9A7) </span>

---

### Thursday 19/10

* 09:00-09:20 - **Distributing Computations** - Farming independent runs [Ali Farnudi]
* 09:20-09:30 - **Distributing Computations** - Computational resources @ENS de Lyon [Guillaume Cerutti]
* 09:30-10:00 - **Distributing Computations** - Threads, processes and synchronization [Arthur Luciani]
* 10:00-10:30 - **Distributing Computations** - CPU parallelism in Python [Ali Farnudi]
* <span style="color:cornflowerblue">10:30-11:00 - *Coffee Break*</span>
* <span><!-- .element: class="fragment" data-fragment-index="1" -->11:00-12:30 - *__Invited Speakers__* - [Jonathan Rouzaud-Cornabas, Samuel Bernard] </span>
* <span style="color:cornflowerblue"><!-- .element: class="fragment" data-fragment-index="1" -->12:30-14:00 - *Lunch Break*</span>
* <span style="color:palegreen"><!-- .element: class="fragment" data-fragment-index="2" -->14:00-15:00 - *__Hands-on__* - Are there parallelizable critical algorithms in my own program? </span>
* <span style="color:palegreen"><!-- .element: class="fragment" data-fragment-index="2" --> 15:00-15:30 - *__Hands-on__* - Highlighting "good examples" </span>
* <span style="color:orangered"><!-- .element: class="fragment" data-fragment-index="3" -->16:00-17:30 - **_Social activity_** - Laser Game @[MegaZone](https://maps.app.goo.gl/8YYoJNmxXGyZreKo8) </span>

---

### Friday 20/10



* 09:00-09:30 - **Implementing Parallelism** - Real-life example: `Julia` [Ibrahim Cheddadi]
* 09:30-10:00 - **Implementing Parallelism** - Real-life example: `Numba` [Guillaume Mestdagh]
* 10:00-10:30 - **Implementing Parallelism** - Real-life example: `ctypes`/`scipy` [André-Claude Clapson]
* <span style="color:cornflowerblue">10:30-11:00 - *Coffee Break*</span>
* 11:00-11:30 - **Implementing Parallelism** - Real-life example: `MatLab` [Manuel Petit]
* 11:30-12:00 - **Implementing Parallelism** - Feedback on GPU training and `OpenCL` [Guillaume Cerutti]
* 12:00-12:30 - **Implementing Parallelism** - Real-life example: `OpenMM` + `timagetk` [Ali Farnudi]
* <span style="color:cornflowerblue">12:30-14:00 - *Lunch Break*</span>
* <span  style="color:palegreen"><!-- .element: class="fragment" data-fragment-index="1" -->14:00-15:30 - *__Discussions__* - What strategy for HPC in the team?</span>
* <span style="color:cornflowerblue"><!-- .element: class="fragment" data-fragment-index="1" -->15:30-16:00 - *Coffee Break*</span>
* <span style="color:gold"><!-- .element: class="fragment" data-fragment-index="2" -->16:00-17:30 - **_Meta-scientific activity_** - Integrity in science [Christophe Godin, Jeanne Abitbol-Spangaro, Landry Duguet, Jonathan Legrand]</span>
