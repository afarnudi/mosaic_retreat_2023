"""Use example to compare the execution time of arrays and lists in loops, numpy methods, functional style, etc


Use the following command to get the execution time:
python -m timeit -s "import list_arr_timing as M" "M.[function name](M.[data structure])"

you can replace [function name] with the following functions:
ranged_forloop
indexed_forloop
functional_plain
functional_numpy
dotprod
filter_usage

and replace [data structure] with either of the following datatypes:
xlst    python list
xarr    numpy array

Example to get the execution time of the ranged_forloop on a python list:
python -m timeit -s "import list_arr_timing as lat" "lat.ranged_forloop(lat.xlst)"
"""

import random
import numpy as np

N = 1_000_000
xlst = list(random.uniform(-10.0, 10.0) for _ in range(N))
xarr = np.array(xlst, dtype="float64")

ones = np.ones_like(xarr)


def ranged_forloop(xs):
    """Calculate the sum of the input list.

    Iterating over the list content.

    Parameters
    ----------
    xs : Iterable list
        The list of items to sum.

    Returns
    -------
    float
        Sum of numbers in input list
    """
    total = 0.0
    for i in xs:
        total += abs(i)
    return total


def indexed_forloop(xs):
    """Calculate the sum of the input list.

    Call each element using the index and get the sum.

    Parameters
    ----------
    xs : Iterable list
        The list of items to sum.

    Returns
    -------
    float
        Sum of numbers in input list
    """
    total = 0.0
    L = len(xs)
    for i in range(L):
        total += abs(xs[i])
    return total


def functional_plain(xs):
    """Calculate the sum of the absolute value of the input list.

    Map the abs function on all list elements and use the python sum method to calculate the sum.

    Parameters
    ----------
    xs : Iterable list
        The list of items to sum.

    Returns
    -------
    float
        Sum of numbers in input list
    """
    return sum(map(abs, xs))


def functional_numpy(xs):
    """Calculate the sum of the absolute value of the input list.

    Use numpy methods to calculate the sum of the absolute value of input contents.

    Parameters
    ----------
    xs : Iterable list
        The list of items to sum.

    Returns
    -------
    float
        Sum of numbers in input list
    """
    return np.sum(np.abs(xs))


def dotprod(xs):
    """Calculate the dot product of the absolute value of the input vector with a unit vector.

    Use numpy methods to calculate the dot product with a unit vector with the same dimension.

    Parameters
    ----------
    xs : Iterable list
        The list of items to sum.

    Returns
    -------
    float
        dot product.
    """
    return np.dot(np.abs(xs), ones)


def filter_usage(xs):
    """Use filters to multiply the content of input list that is between 7 and 8.

    Parameters
    ----------
    xs : Iterable list
        The list of items to sum.
    """
    xs = np.asarray(xs)
    # make a boolean array of True/False values
    filter = (xs > 7) & (xs < 8)
    # use the array as index to reference the matching elements
    xs[filter] *= 1000


def list_comp(xs):
    """Calculate the sum of the absolute value of the input list.

    Make a list of absolute values with list comprehension and return sum of the list.

    Parameters
    ----------
    xs : Iterable list
        The list of items to sum.

    Returns
    -------
    float
        Sum of numbers in input list
    """
    return sum([abs(_) for _ in xs])


def list_comp_no_brack(xs):
    """Calculate the sum of the absolute value of the input list.

    Use Python list comprehension to calculate the sum of the absolute values of the input list.

    Parameters
    ----------
    xs : Iterable list
        The list of items to sum.

    Returns
    -------
    float
        Sum of numbers in input list
    """
    return sum(abs(_) for _ in xs)
