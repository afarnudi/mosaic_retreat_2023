---
type: slide
title: Mosaic Team Retreat 2023
slideOptions:
  transition: 'fade'
  center: true
  width: 1900
  height: 1000


---

# Mosaic Team Retreat 2023
## Towards High(er) Performance Computing

<!-- .slide: data-background="linear-gradient(to bottom, #283b95, #222222)" -->

---

### Wednesday 18/10
* <span style="color:cornflowerblue">09:00-09:30 - *Welcome*</span>
* 09:30-10:00 - **Introduction** - When to optimize a program and how? **[AF]**
* 10:00-10:30 - **Optimize First** - Choosing appropriate data structures: list vs array **[AF]**
* <span style="color:cornflowerblue">10:30-11:00 - *Coffee Break*</span>
* 11:00-12:00 - **Optimize First** - Complexity and optimization: what makes a program slow? **[AL]**
* 12:00-12:30 - **Optimize First** - Profiling: looking for computational bottlenecks **[GC]**
* <span style="color:cornflowerblue">12:30-14:00 - *Lunch Break*</span>
* <span style="color:palegreen"><!-- .element: class="fragment" data-fragment-index="1" -->14:00-15:30 - *__Hands-on__* - Testing profiling tools on my own programs</span>
* <span style="color:cornflowerblue"><!-- .element: class="fragment" data-fragment-index="1" -->15:30-16:00 - *Coffee Break*</span>
* <span style="color:gold"><!-- .element: class="fragment" data-fragment-index="2" -->16:00-17:30 - **_Meta-scientific activity_** - Note-taking / journaling: practices and tools **[GC]**</span>
* <span style="color:orangered"><!-- .element: class="fragment" data-fragment-index="3" -->18:00-21:30 - **_Social activity_** - Dinner & Games @ Tire-toi une bûche</span>

---

### Thursday 19/10

* 09:00-09:30 - **Distributing Computations** - Farming independent runs (+ Calculus, PSMN) **[AF]**
* 09:30-10:00 - **Distributing Computations** - Threads, processes and synchronization **[AL]**
* 10:00-10:30 - **Distributing Computations** - CPU parallelism in Python **[AF]**
* <span style="color:cornflowerblue">10:30-11:00 - *Coffee Break*</span>
* <span><!-- .element: class="fragment" data-fragment-index="1" -->11:00-12:30 - *__Invited Speaker__* - J. Rouzaud-Cornabas - TBA </span>
* <span style="color:cornflowerblue"><!-- .element: class="fragment" data-fragment-index="1" -->12:30-14:00 - *Lunch Break*</span>
* <span style="color:palegreen"><!-- .element: class="fragment" data-fragment-index="2" -->14:00-15:00 - *__Hands-on__* - Are there parallelizable critical algorithms in my own program? </span>
* <span style="color:palegreen"><!-- .element: class="fragment" data-fragment-index="2" --> 15:00-15:30 - *__Hands-on__* - Highlighting "good examples" </span>
* <span style="color:cornflowerblue"><!-- .element: class="fragment" data-fragment-index="2" -->15:30-16:00 - *Coffee Break*</span>
* <span style="color:orangered"><!-- .element: class="fragment" data-fragment-index="3" -->16:00-17:30 - **_Social activity_** - Laser Game @ MegaZone</span>

---

### Friday 20/10

* 09:00-09:45 - **Implementing Parallelism** - Experience & Feedback on `Julia` **[IC]**
* 09:45-10:30 - **Implementing Parallelism** - Experience & Feedback on `Numba` **[GM]**
* <span style="color:cornflowerblue">10:30-11:00 - *Coffee Break*</span>
* 11:00-11:45 - **Implementing Parallelism** - Experience & Feedback on `OpenCL` **[GC]**
* 11:45-12:30 - **Implementing Parallelism** - Introducing `Dask` (and/or `Rapids`?) **[AF]**
* <span style="color:cornflowerblue">12:30-14:00 - *Lunch Break*</span>
* <span  style="color:palegreen"><!-- .element: class="fragment" data-fragment-index="1" -->14:00-15:30 - *__Discussions__* - What strategy for HPC in the team?</span>
* <span style="color:cornflowerblue"><!-- .element: class="fragment" data-fragment-index="1" -->15:30-16:00 - *Coffee Break*</span>
* <span style="color:gold"><!-- .element: class="fragment" data-fragment-index="2" -->16:00-17:30 - **_Meta-scientific activity_** - Ethics in science (~@RDPiades) **[CG, ??, ??]**</span>

---

## Introduction

### When to optimize a program and how? (~30-40 min)
* When do you need to start worrying about computational time?
* Real-life examples of problematic programs
* Overview of ways to optimize (the execution time of) a program
    * Do less, use more efficient functions / memory tradeoff
    * Mention parallelism as one solution -> hardware requirements
* Do you actually need parallelism?

---

## Optimize First

### Basic code optimizations

#### Lists vs arrays (Ali) (~20-30 min)
* `timeit`

#### Complexity of basic operations on common data structures (Arthur) (~20-30 min)
* Appending an element to a dict / list / array

#### Memory access time issues
* network vs disk vs RAM vs cache (memory architecture)

### Profiling (Guillaume) (30min + 1h hands-on)
* `cProfile`
* Identify bottlenecks in people's own code?

---

## Process distribution

### "Farming" 
* Independent runs of the same program: through python / or through bash

### Theory: processes / threads / synchronization

### Multiple processes in `make -j` compilation

## CPU Parallelism in Python (`numpy`, `multiprocessing`, `concurrent`)

### Simple `multiprocessing` example (Ali) (~30 min)
* auto parallelism in `numpy`
* rely on existing solutions when possible!

### `concurrent.futures` example?

---


## Coding parallel algorithms

### Back to profiling
* Identify critical algorithms
* Is it paralellizable?

### Condensed OpenCL example

### (Handling big data)
* `dask` / `rapids` for processing chunks of data

### Julia programming / Using Numba
* Guillaume M.? Ibrahim?

### André-Claude?

### Corentin -> Carbon cost of CPU usage?

---

## Agenda


|             | Wed. 18/10                                | Thu. 19/10                                                | Fri. 20/10                                      |
| ----------- | ----------------------------------------- | --------------------------------------------------------- | ----------------------------------------------- |
| 09:00-10:30 | Introduction <br> Basic Code Optimization | Process distribution: Farming + multiprocessing/threading | Julia *Ibrahim*                                 |
| 10:30-12:00 | Profiling: tool presentation              | *J. Rouzaud Cornabas*                                     | Numba *Guillaume M.* <br> OpenCL *Guillaume C.* |
| 14:00-15:30 | Profiling hands-on?                       | Hands-on identifying critical algorithms: parallelizable? | Hands-on roundtables (1 per library/language)   |
| 15:30-17:00 | *Meta-scientific activity*                | *Meta-scientific activity*                                | *Discussions?*                                  |
