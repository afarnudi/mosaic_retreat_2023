#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Parallel execution of a programme.

Use concurrent.futures to submit jobs and print results when finished.
"""
import concurrent.futures
import time
import numpy as np
import random

from tools import do_something_string_return


def main():
    start = time.perf_counter()
    secs = np.arange(20)[::-1]
    # print(secs.reshape((2,10)))
    with concurrent.futures.ProcessPoolExecutor() as executor:
        results = [
            executor.submit(do_something_string_return, f_input) for f_input in secs
        ]

        for i, f in enumerate(concurrent.futures.as_completed(results)):
            print(i + 1, f.result())

    finish = time.perf_counter()
    print(f"finished in {round(finish-start,2)} second(s).")


if __name__ == "__main__":
    main()
