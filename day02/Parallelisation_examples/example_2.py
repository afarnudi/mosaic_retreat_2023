#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 19:39:50 2022

@author: ali
"""
import multiprocessing
import time
import matplotlib.pyplot as plt
import numpy as np


def sum_function(beg, end, skip):
    sum_0 = 0
    for i in range(beg, end, skip):
        sum_0 += i
    return sum_0


def main_sum_serial(end):
    start = time.process_time()

    result = sum_function(0, end, 1)

    finish = time.process_time()
    print("serial:")
    print(result)
    print(f"Took {round((finish-start),2)} second(s).\n")


def main_sum_parallel(end):
    start = time.perf_counter()

    max_processes = 10

    begs = [i for i in range(max_processes)]
    ends = [end for i in range(max_processes)]
    skips = [max_processes for i in range(max_processes)]
    with multiprocessing.Pool(processes=max_processes) as pool:
        results = pool.starmap(sum_function, zip(begs, ends, skips))

    finish = time.perf_counter()

    print("parallel:")
    print(np.sum(results))
    print(f"Took {round((finish-start),2)} second(s).\n")


def main_sum_numpy(end):
    start = time.process_time()

    result = np.sum(np.arange(end))

    finish = time.process_time()
    print("numpy:")
    print(result)
    print(f"Took {round((finish-start),2)} second(s).\n")


if __name__ == "__main__":
    end = 1_000_000_001
    main_sum_serial(end)
    main_sum_parallel(end)
    main_sum_numpy(end)
