import time
import os

def do_something(t=1):
    """Ask the computer to do nothing for t seconds

    Parameters
    ----------
    t : float , optional
        Computer wait time in seconds, by default 1
    """
    print("Sleeping for 1 second(s)...")
    time.sleep(t)
    print("Finished sleep.")


def do_something_string_return(t):
    """Ask the computer to do nothing for t seconds

    Parameters
    ----------
    t : int
        Computer wait time in seconds

    Returns
    -------
    str
        End message of the function.
    """
    time.sleep(t)
    return f'{os.getpid()} Finshed: {t}'