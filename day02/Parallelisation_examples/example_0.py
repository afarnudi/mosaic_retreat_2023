#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example of serial execution of a programme.

Call a function in sequence to kill time for a couple of seconds and report the total programme runtime.
"""

import time
from tools import do_something


def main():
    start = time.perf_counter()
    do_something()
    do_something()
    do_something()

    finish = time.perf_counter()
    print(f"finished in {round(finish-start,2)} second(s).")


if __name__ == "__main__":
    main()
