#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Parallel execution of a programme.

Use concurrent.futures to submit jobs and print results when finished.
"""
import concurrent.futures
import time
import numpy as np
import random

from tools import do_something_string_return


def main():
    random.seed(0)
    secs = np.arange(20)  # [::-1]
    random.shuffle(secs)
    print(secs)
    output = ""
    start = time.perf_counter()
    with concurrent.futures.ProcessPoolExecutor() as executor:
        results = [executor.submit(do_something_string_return, _) for _ in secs]

        for i, f in enumerate(concurrent.futures.as_completed(results)):
            output += f"{f.result()}\n"

    finish = time.perf_counter()

    print(output)
    print(f"total time: {sum(secs)}")
    print(f"secs in descending order: {sorted(secs)[::-1]}")
    print(
        f"finished in {round(finish-start,2)} second(s). {round(sum(secs)/(finish-start),2)}x speedup"
    )

    # Generate advanced report
    str_results = output.split("\n")
    output = "\nWhich workers did what:\n"

    last_pid = ""
    worker_counter = 0
    for line in sorted(str_results):
        if len(line) != 0:
            pid = line.split()[0]

            if last_pid == pid:
                output += (
                    "   " + " " * len(pid) + f" {line.split()[1]} {line.split()[2]}\n"
                )
            else:
                worker_counter += 1
                output += f"\n{worker_counter}: {line}\n"
                last_pid = output.split()[-3]

    print(output)


if __name__ == "__main__":
    main()
