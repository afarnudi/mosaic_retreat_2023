#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Parallel execution of a programme.

Use lists to distribute processes.
"""
import multiprocessing
import time

from tools import do_something


def main():
    start = time.perf_counter()
    processes = []
    for i in range(100):
        p = multiprocessing.Process(target=do_something)
        processes.append(p)

    for p in processes:
        p.start()

    for p in processes:
        p.join()

    finish = time.perf_counter()
    print(f"finished in {round(finish-start,2)} second(s).")


if __name__ == "__main__":
    main()
