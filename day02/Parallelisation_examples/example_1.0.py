#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Parallel execution of a programme.
"""
import multiprocessing
import time

from tools import do_something


def main():
    start = time.perf_counter()

    p1 = multiprocessing.Process(target=do_something)
    p2 = multiprocessing.Process(target=do_something)
    p3 = multiprocessing.Process(target=do_something)

    p1.start()
    p2.start()
    p3.start()

    # join is used to make the current python programme wait for processes
    # to finish before proceeding with the rest of the code.
    p1.join()
    p2.join()
    p3.join()

    finish = time.perf_counter()
    print(f"finished in {round(finish-start,2)} second(s).")


if __name__ == "__main__":
    main()
