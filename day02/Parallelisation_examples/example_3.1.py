#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from itertools import repeat
import multiprocessing
import time
import matplotlib.pyplot as plt
import numpy as np


def pi_mc(seed, num_trials=50_000):
    """Estimate the number pi through the metropolis algorithm.

    the ratio of the area of a quarter of a unit circle (pi/4) to the area of a unit square
    is calculated by the ratio of the number of random coordinates inside a unit square that
    lie inside the unit circle quarter to the total number of random coordinates.

    Parameters
    ----------
    seed : int
        random number generator seed.
    num_trials : int, optional
        The number of generated random coordinates, by default 50_000

    Returns
    -------
    float
        Estimate of the number pi
    """
    counter = 0
    np.random.seed(seed)

    for j in range(num_trials):
        x_val = np.random.random_sample()
        y_val = np.random.random_sample()

        radius = x_val**2 + y_val**2

        if radius < 1:
            counter += 1

    return 4 * counter / num_trials


def main_benchmark(number_of_samples, max_processes=12):
    """benchmark the performance of the function pi_mc with different number of processes.

    Parameters
    ----------
    number_of_samples : int
        The number of samples to use to average the execution time of the pi_mc using n-processes
    max_processes : int
        The maximum number of processes to use for the benchmark, default 12
    """
    execution_times_avg = []
    execution_times_err = []

    seed_array = list(range(50))

    for num_of_processors in range(1, max_processes):
        n_process_time = []
        for _ in range(number_of_samples):
            start = time.perf_counter()

            with multiprocessing.Pool(processes=num_of_processors) as pool:
                result = pool.map(pi_mc, seed_array)
            finish = time.perf_counter()
            n_process_time.append(finish - start)
        execution_times_avg.append(round(np.mean(n_process_time), 3))
        execution_times_err.append(
            np.std(n_process_time) / np.sqrt(number_of_samples - 1)
        )

    plt.errorbar(
        range(1, max_processes),
        execution_times_avg,
        yerr=execution_times_err,
        capsize=2,
        fmt="o:",
    )
    plt.xlabel("# of processes")
    plt.ylabel("execution time [seconds]")
    plt.show()


if __name__ == "__main__":
    number_of_samples = 5
    # start = time.perf_counter()
    main_benchmark(number_of_samples, max_processes=16)
    # finish = time.perf_counter()
    # print(round(finish-start,2))
