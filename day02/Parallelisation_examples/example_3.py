#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from itertools import repeat
import multiprocessing
import time
import matplotlib.pyplot as plt
import numpy as np


def pi_mc(seed, num_trials=500_000):
    """Estimate the number pi through the metropolis algorithm.

    the ratio of the area of a quarter of a unit circle (pi/4) to the area of a unit square
    is calculated by the ratio of the number of random coordinates inside a unit square that
    lie inside the unit circle quarter to the total number of random coordinates.

    Parameters
    ----------
    seed : int
        random number generator seed.
    num_trials : int, optional
        The number of generated random coordinates, by default 50_000

    Returns
    -------
    float
        Estimate of the number pi
    """
    counter = 0
    np.random.seed(seed)

    for j in range(num_trials):
        x_val = np.random.random_sample()
        y_val = np.random.random_sample()

        radius = x_val**2 + y_val**2

        if radius < 1:
            counter += 1

    return 4 * counter / num_trials


def main(number_of_samples):
    """Estimate the number pi.

    Parameters
    ----------
    number_of_samples : int
        The number of seeds used to perform Monte Carlo estimations of the number pi
    """
    start = time.process_time()
    results = []
    for seed in range(number_of_samples):
        results.append(pi_mc(seed))

    print(np.mean(results))
    finish = time.process_time()
    print("sequential:")
    print(f"Took {round((finish-start),2)} second(s).")
    print(f"{round((finish-start)/number_of_samples,2)} second(s) per sample.")


def main_parallel(number_of_samples):
    """Estimate the number pi by distributing seed calculations.

    Parameters
    ----------
    number_of_samples : int
        The number of seeds used to perform Monte Carlo estimations of the number pi
    """
    print("============\n")
    start = time.perf_counter()

    seed_array = list(range(number_of_samples))
    with multiprocessing.Pool() as pool:
        result = pool.map(pi_mc,seed_array)
    print(np.mean(result))
    finish = time.perf_counter()
    print("parallel:")
    print(f"Took {round((finish-start),2)} second(s).")


if __name__ == "__main__":
    number_of_samples = 30

    main(number_of_samples)
    main_parallel(number_of_samples)
